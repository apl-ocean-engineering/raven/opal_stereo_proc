<?xml version="1.0"?>

<!-- Most generic "top-level" launchfile for running stereo.
     This launchfile can _either_ launch the stereo stack
     **or** chain out to a docker image -->

<launch>

  <arg name="run_docker" default="false"
      doc="If true, launchfile will try to start a docker image (run_in_docker.sh).
           Note stereo_matcher must be set separately to enable GPU-accelerated stereo_image_proc." />

  <!-- ~~~~~~ -->

  <arg name="stereo_ns"
       default="/raven/stereo"
       doc="The 'root' namespace for all stereo processings" />

  <!-- === Stereo block matching configuration == -->
  <arg name="run_stereo" default="true" />

  <!-- Valid values are:
           "ros"   uses ROS stereo_image_proc
           "vpi"   Uses VPI (required Nvidia libraries) -->
  <arg name="stereo_matcher" default="ros" />

  <!-- "downsample" is given as a power of 2 (e.g. downsample = 2 means scale by 4)-->
  <arg name="downsample" default="2" if="$(eval stereo_matcher=='ros')"/>    <!-- Default to 4x decimation for CPU -->
  <arg name="downsample" default="1" if="$(eval stereo_matcher=='vpi')"/>    <!-- Default to 2x decimation for GPU -->

  <!-- Matcher params are loaded from yaml files.  By default, load from this directory -->
  <arg name="gpu_stereo_image_proc_params" default="$(dirname)/gpu_stereo_image_proc_params.yaml"/>
  <arg name="stereo_image_proc_params" default="$(dirname)/stereo_image_proc_params.yaml"/>

  <!-- === Create various disparity-derived products? === -->

  <!-- I don't want to make the disparity_visualize repo an explicit dependency for this
       repo, so it is enabled by default for docker and disabled by
       default for local -->
  <arg name="run_disparity_visualize" default="$(arg run_docker)" />

  <!-- === Create point2 pointcloud? === -->
  <!-- As of right now, we're running the node version of stereo_image_proc which includes
       point cloud generation.   So disable it by default for "ros".
       This may change in the future if we go to a nodelet-based configuration.  -->
  <arg name="run_point_cloud" default="true" if="$(eval stereo_matcher=='vpi')" />
  <arg name="run_point_cloud" default="false" if="$(eval stereo_matcher=='ros')" />

  <!-- === Run web_video_server? === -->
  <arg name="run_web_video_server" default="false"/>
  <arg name="web_video_server_port" default="8080"/>

  <!-- ===~~== Main if(docker) starts here ==~~=== -->

  <!-- Run Docker version (this is slightly painful)-->
  <node if="$(arg run_docker)"
        name="opal_stereo_proc_docker"
        pkg="opal_stereo_proc"
        type="run_in_docker.sh"
        output="screen"
        args="run_docker:=false
              stereo_ns:=$(arg stereo_ns)
              run_stereo:=$(arg run_stereo)
              stereo_matcher:=$(arg stereo_matcher)
              downsample:=$(arg downsample)
              run_disparity_visualize:=$(arg run_disparity_visualize)
              run_point_cloud:=$(arg run_point_cloud)
              run_web_video_server:=$(arg run_web_video_server)
              web_video_server_port:=$(arg web_video_server_port)" />

  <!-- Or, run local version -->
  <include unless="$(arg run_docker)"
        file="$(dirname)/opal_stereo_proc_local.launch"
        pass_all_args="true" />

</launch>

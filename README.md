# Raven_stereo_proc

ROS meta-package for the "canonical" OPAL stereo processing stack.  It contains best-practices launchfiles and params for both:

* The "standard" ROS [`stereo_image_proc`](http://wiki.ros.org/stereo_image_proc), based on the original Raven stereo configuration.   This is for **cases where you don't have a GPU.**
* An "accelerated" [`gpu_stereo_image_proc`](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/jetpack-5.1.2) based on the Trisect stereo image system.  This requires an NVidia GPU.

Given the heavy dependencies, the `gpu_stereo_image_proc` version is designed to be run in Docker.   This script contains an entry script for launching a Docker as part of a ROS environment (see below).

The non-GPU version can be run either in Docker or in a "normal" ROS environment.

If this repo is added to a Catkin workspace, all startup options can be accessed through the `opal_stereo_proc.launch` launchfile.

# 1. Running GPU-accelerated stereo in Docker

!! [NVidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) must be installed and you must have an Nvidia GPU.


Run:

```
roslaunch opal_stereo_proc opal_stereo_proc.launch run_docker:=true stereo_matcher:=vpi
```

The script loads stereo parameters from the `launch/*.yaml` files in this repo.   These can be modified to update defaults without rebuilding the Docker image.

# 2. Running non-accelerated stereo in Docker

Run:

```
roslaunch opal_stereo_proc opal_stereo_proc.launch run_docker:=true
```


# 3. Running non-accelerated stereo outside of Docker

Add this repo to a ROS workspace, catkin build and:

```
roslaunch opal_stereo_proc opal_stereo_proc.launch
```


# Launchfiles

This repo wraps multiple modes of operation behind a single Launchfile.   The overall flow is documented below:

![](figures/launchfile_flow.svg)


# Manual Docker instructions

The Docker configuration is in the `.docker/` directory.   To rebuild the docker image:

```
docker compose build opal_stereo_proc
```

The `docker-compose` defines a `dev` image which mounts the current repo for development.   To use:

```
UID=`id -u` GID=`id -g` docker compose build dev
docker compose run dev
```

See `docker-compose.yml` for examples on how to mount other ROS packages for development.


A current Docker image is published in the [container registry for the opal_stereo_proc repo](https://gitlab.com/apl-ocean-engineering/raven/opal_stereo_proc/container_registry).   `docker compose pull` should pull this image.

Note this image is build and pushed by hand (not CI), right now.

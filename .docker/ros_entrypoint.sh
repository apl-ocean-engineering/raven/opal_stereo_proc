#!/bin/bash
set -e

ROS_VERSION=${ROS_VERSION:-noetic}

echo "Using ROS_MASTER_URI=$ROS_MASTER_URI"

# setup ros environment
ROS_WS_SETUP="$ROS_WS/devel/setup.bash"
if [[ -f  "$ROS_WS_SETUP" ]]; then
    echo "Loading environment from workspace: $ROS_WS_SETUP"
    source "$ROS_WS_SETUP"
else
    echo "Loading default environment: /opt/ros/$ROS_VERSION/setup.bash"
    source /opt/ros/$ROS_VERSION/setup.bash
fi

exec "$@"

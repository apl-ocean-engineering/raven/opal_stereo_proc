#!/bin/bash
#
# This is the "userspace" entrypoint for running the stereo_image_proc

# It mounts ../launch/ to /launch in the Docker image
# and runs opal_stereo_proc.launch with all ROS params on the commands line

LAUNCH_DIR=`rospack find opal_stereo_proc`/launch
DOCKER_DIR=`rospack find opal_stereo_proc`/.docker

if [[ ! -d $LAUNCH_DIR ]]; then
        echo "Hm, couldn't find the opal_stereo_proc/launch directory"
        exit -1
fi

docker compose -f $DOCKER_DIR/docker-compose.yml \
        run -v $LAUNCH_DIR:/launch \
        opal_stereo_proc \
        roslaunch /launch/opal_stereo_proc.launch \
                       stereo_image_proc_params:=/launch/stereo_image_proc_params.yaml \
                       gpu_stereo_image_proc_params:=/launch/gpu_stereo_image_proc_params.yaml \
                        $@
